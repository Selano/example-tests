#include "../include/classA.hpp"
#include "../include/funcs.hpp"
#include "../include/classB.hpp"
#include "../include/classC.hpp"
#include <gtest/gtest.h>
#include <vector>
#include <functional>
#include <algorithm>

TEST(funcTests, commonFunc) {
    // Arrange
    std::vector<int> rawVector = {3, 1, 12, 10, 6, 5}; 
    std::vector<int> sortVector = {1, 3, 5, 6, 10, 12};
    A objectCommon(rawVector); 

    // Act
    objectCommon.sort(std::bind(sort_func_common, std::placeholders::_1));

    // Assert
    ASSERT_EQ(objectCommon.get(), sortVector);
}

TEST(funcTests, staMetodTest) {
    // Arrange
    std::vector<int> rawVector = {3, 1, 12, 10, 6, 5}; 
    std::vector<int> sortVector = {1, 3, 5, 6, 10, 12};
    A objectCommon(rawVector); 
    B objectB;

    // Act
    objectCommon.sort(std::bind(&(objectB.staticSort), std::placeholders::_1));

    // Assert
    ASSERT_EQ(objectCommon.get(), sortVector);
}

TEST(funcTests, metodTest) {
    // Arrange
    std::vector<int> rawVector = {3, 1, 12, 10, 6, 5}; 
    std::vector<int> sortVector = {1, 3, 5, 6, 10, 12};
    A objectCommon(rawVector); 
    B objectB;

    // Act
    objectCommon.sort(std::bind(&B::sort, &objectB, std::placeholders::_1));

    // Assert
    ASSERT_EQ(objectCommon.get(), sortVector);
}

TEST(funcTests, functorTest) {
    // Arrange
    std::vector<int> rawVector = {3, 1, 12, 10, 6, 5}; 
    std::vector<int> sortVector = {1, 3, 5, 6, 10, 12};
    A objectCommon(rawVector); 
    functor objfunc;

    // Act
    objectCommon.sort(objfunc);

    // Assert
    ASSERT_EQ(objectCommon.get(), sortVector);
}