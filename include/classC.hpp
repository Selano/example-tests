#pragma once

#include <iostream>
#include <algorithm>
#include <vector>

class functor {
    public:
        void operator()(std::vector<int>&);
};