#pragma once

#include <iostream>
#include <functional>
#include <vector>

class A {
    private:
        A(const A&) = delete;
        A& operator=(const A&) = delete;

        std::vector<int> array;

    public:
        A();
        A(std::vector<int> array);
        void sort(std::function<void (std::vector<int>&)>);
        void set(std::vector<int>);
        std::vector<int> get();
};