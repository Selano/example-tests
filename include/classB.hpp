#pragma once

#include <iostream>
#include <algorithm>
#include <vector>

class B {
    public:
        static void staticSort(std::vector<int>&);
        void sort(std::vector<int>&);
};
