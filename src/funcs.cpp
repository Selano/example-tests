#include "../include/funcs.hpp"

void sort_func_common(std::vector<int>& array) {
    std::cout << "Sorting values in array (common)" << std::endl;
    std::sort(array.begin(), array.end());
}