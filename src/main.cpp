#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include "../include/classA.hpp"
#include "../include/classB.hpp"
#include "../include/classC.hpp"
#include "../include/funcs.hpp"

int main() {
    std::cout << "Common function example" << std::endl << std::endl;

    A objectCommon;
    objectCommon.sort(std::bind(sort_func_common, std::placeholders::_1));
    std::cout << std::endl;

    std::cout << "Lambda example" << std::endl << std::endl;

    auto sort_func_lambda = [] (std::vector<int>& array) -> void {
        std::cout << "Sorting values in array (lambda)" << std::endl;
        std::sort(array.begin(), array.end());
    };    

    A objectLambda;
    objectLambda.sort(std::bind(sort_func_lambda, std::placeholders::_1));
    std::cout << std::endl;

    B objectB;

    std::cout << "Class metod example(static metod)" << std::endl << std::endl;

    A objectSMetod;
    objectSMetod.sort(std::bind(&(objectB.staticSort), std::placeholders::_1));
    std::cout << std::endl;

    std::cout << "Class metod example(metod)" << std::endl << std::endl;

    A objectMetod;
    objectMetod.sort(std::bind(&B::sort, &objectB, std::placeholders::_1));
    std::cout << std::endl;

    std::cout << "Class metod example(functor)" << std::endl << std::endl;

    functor objfunc;
    A objectFMetod;
    objectFMetod.sort(objfunc); // std::bind(objfunc, std::placeholders::_1)
    std::cout << std::endl;
}