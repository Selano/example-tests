#include "../include/classB.hpp"

void B::staticSort(std::vector<int>& array) {
    std::cout << "Sorting values in array (static metod)" << std::endl;
    std::sort(array.begin(), array.end());
}

void B::sort(std::vector<int>& array) {
    std::cout << "Sorting values in array (metod)" << std::endl;
    std::sort(array.begin(), array.end());
}