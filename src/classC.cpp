#include "../include/classC.hpp"

void functor::operator()(std::vector<int>& array) {
    std::cout << "Sorting values in array (functor)" << std::endl;
    std::sort(array.begin(), array.end());
}