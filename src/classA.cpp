#include "../include/classA.hpp"

A::A() {
    for(int i = 0; i < 10; i++) {
        this->array.push_back(rand() % 100);
    }
}

A::A(std::vector<int> array) {
    this->array = array;
}

void A::set(std::vector<int> array) {
    this->array = array;
}

std::vector<int> A::get() {
    return array;
}


void A::sort(std::function<void (std::vector<int>&)> sortfunc) {
    std::cout << "Start sorting in A" << std::endl;
    std::cout << "Value: ";
    for(int val : this->array) {
        std::cout << val << " ";
    }
    std::cout << std::endl;

    sortfunc(this->array);

    std::cout << "End sorting in A" << std::endl;
    std::cout << "Value: ";
    for(int val : this->array) {
        std::cout << val << " ";
    }
    std::cout << std::endl;
}